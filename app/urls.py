from django.urls import path 
from . import views 
 
app_name = 'app' 
urlpatterns = [
    path('', views.index, name='index'), 
    path('admin/', views.admin, name='admin'),
    path('adminestudiante/', views.adminestudiante, name='adminestudiante'),
    path('adminestudiante/crear/', views.adminestudiantecrear, name='adminestudiantecrear'),
    path('editarestudiante/<int:id>/', views.editarestudiante, name='editarestudiante'),
    path('admingrupo/', views.admingrupo, name='admingrupo'),
    path('admingrupo/crear/', views.admingrupocrear, name='admingrupocrear'),
    path('adminvergrupo/<int:id>/', views.adminvergrupo, name='adminvergrupo'),
    path('admineditargrupo/<int:id>/', views.admineditargrupo, name='admineditargrupo'),
    path('eliminarestudiante/<int:id>/', views.eliminarestudiante, name= 'eliminarestudiante'),
    path('agregarestudiante/<int:id>/', views.agregarestudiante, name= 'agregarestudiante'),
    path('adminagregarestudiantegrupo/<int:id>/', views.adminagregarestudiantegrupo, name= 'adminagregarestudiantegrupo'),
    path('adminprofesores/', views.adminprofesores, name='adminprofesores'),
    path('adminprofesores/crear/', views.adminprofesorescrear, name='adminprofesorescrear'),
    path('admineditestu/<int:id>/', views.admineditestu, name='admineditestu'),
    path('adminverprofesor/<int:id>/', views.adminverprofesor, name='adminverprofesor'),
    path('profesor/', views.profesor, name='profesor'),
    path('profveract/', views.profveract, name='profveract'),
    path('profveract/crear/', views.profveractcrear, name='profveractcrear'),
    path('veractividad/<int:id>/', views.veractividad, name='veractividad'),
    path('profnota/<int:id1>/<int:id2>/', views.profnota, name='profnota'),
    path('guardarnota/<int:id1>/<int:id2>/', views.guardarnota, name='guardarnota'),
    path('estudiantes/', views.estudiantes, name='estudiantes'),
    path('estugrupo/<int:id>/', views.estugrupo, name='estugrupo'),
    path('proflistagrupos/', views.proflistagrupos, name='proflistagrupos'),
    path('profactgrupo/', views.profactgrupo, name='profactgrupo'),
    path('profvergrupo/<int:id>/', views.profvergrupo, name='profvergrupo'),
    path('profeactynota/<int:id>', views.profeactynota, name='profeactynota'), 
    path('login/', views.form_login, name='form_login'),
    path('iniciar_sesion/', views.iniciar_sesion, name="iniciar_sesion"),
    path('logout/', views.cerrar_sesion, name='cerrar_sesion'),

]