from django.db import models
from django.contrib.auth.models import User

class Estudiante(models.Model):
    codigo = models.CharField(max_length=20, null=False)
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        # related_name='informacion',
        primary_key=True,
    )
    
    def __str__(self):
        return self.codigo 

    class Meta:
        app_label = 'app'



class Profesor(models.Model):
    catedra = models.BooleanField(null=True)
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        primary_key=True,
    )

    def __str__(self):
        return self.user.first_name

    class Meta:
        app_label = 'app'



class Grupo(models.Model):
    codigo = models.CharField(max_length=50, null=False)
    asignatura = models.CharField(max_length=100, null=False)
    semestre = models.CharField(max_length=20, null=False)
    profesor = models.ForeignKey(
        Profesor,
        related_name = 'grupos',
        null = True,
        on_delete=models.PROTECT
    )
    

    def __str__(self):
        return self.codigo 

    class Meta:
        app_label = 'app'
        

class EstudianteGrupo(models.Model):
    estudiante = models.ForeignKey(
        Estudiante,
        related_name = 'grupos_estudiantes',
        null = False,
        on_delete=models.PROTECT
    ) 
    grupo = models.ForeignKey(
        Grupo,
        related_name = 'estudiante_grupos',
        null = False,
        on_delete=models.PROTECT
    )

    class Meta:
        app_label = 'app'


class Actividad(models.Model):
    descripcion = models.CharField(max_length=100, null=False)
    grupo = models.ForeignKey(
        Grupo,
        related_name = 'actividad_grupos',
        null = False,
        on_delete=models.PROTECT
    )
    fecha_creacion = models.DateField(auto_now=False, 
                                  auto_now_add=False)

    def __str__(self):
        return self.descripcion
    
    class Meta:
        app_label = 'app'


class Nota(models.Model):
    valor = models.DecimalField(max_digits=3, decimal_places=2)
    estudiantegrupo = models.ForeignKey(
        EstudianteGrupo,
        related_name = 'nota_estudiantegrupos',
        null = False,
        on_delete=models.PROTECT
    )
    actividad = models.ForeignKey(
        Actividad,
        related_name = 'nota_actividadgrupos',
        null = False,
        on_delete=models.PROTECT
    )

    def __str__(self):
        return self.valor 
    
    class Meta:
        app_label = 'app'




    
