from django.shortcuts import render, redirect
from django.http import HttpResponse
from app.models import Estudiante
from app.models import Grupo
from app.models import Profesor
from app.models import Actividad
from app.models import Nota
from app.models import EstudianteGrupo
from django.contrib.auth.models import User	
from django.contrib.auth import authenticate, login, logout
from django.db.models import Q

 
def index(request):
    #return HttpResponse("Bienvenidos")
    return render(request, 'app/index.html')

def admin(request):
    return render(request, 'app/admin.html')


def adminestudiante(request):
    lista_estudiantes =  Estudiante.objects.all() 
    contexto = {
         'estudiantes': lista_estudiantes
     }
    
    return render(request,'app/adminestudiante.html', contexto)

def adminestudiantecrear(request):
    nombre = request.POST['nombre']
    apellido = request.POST['apellido']
    email = request.POST['email']
    codigo = request.POST['codigo']
    documento = request.POST['documento']
    username = request.POST['username']
    usuario = User()
    usuario.first_name = nombre
    usuario.last_name = apellido
    usuario.email = email
    usuario.set_password(documento)
    usuario.username = username
    usuario.save()
    estudiante = Estudiante()
    estudiante.codigo = codigo
    estudiante.user = usuario
    estudiante.save()

    return redirect('app:adminestudiante')


def admineditestu(request, id):
    estudiante = Estudiante.objects.get(user_id=id)          
    contexto = {
        'info_estudiante': estudiante
             
    }
    return render(request, 'app/admineditestu.html', contexto) 

def editarestudiante(request, id):
    estudiante = User.objects.get(id=id)
    nombre = request.POST['nombre']
    apellido = request.POST['apellido']
    email = request.POST['email']
    codigo = request.POST['codigo']
    usuario = request.POST['usuario']

    estudiante.first_name = nombre
    estudiante.last_name = apellido
    estudiante.username = usuario
    estudiante.email = email
    estudiante.codigo = codigo
    estudiante.save()

    return redirect('app:adminestudiante')


def admingrupo(request):
    lista_grupos = Grupo.objects.all()
    profesores = Profesor.objects.all()
    contexto = {
        'grup_list': lista_grupos,
        'profesores': profesores
    }
    
    return render(request, 'app/admingrupo.html', contexto)

def admingrupocrear(request):
    profesorid = request.POST['profesor']
    asignatura = request.POST['asignatura']
    codigo = request.POST['codigo']
    semestre = request.POST['semestre']
    grupo = Grupo()
    grupo.codigo = codigo
    grupo.semestre = semestre
    grupo.asignatura = asignatura
    grupo.profesor_id = profesorid
    grupo.save()

    return redirect('app:admingrupo')
    
def adminvergrupo(request, id):
    profesores = Profesor.objects.all()
    grupo = Grupo.objects.get(id=id)
    lista_estudiantes = EstudianteGrupo.objects.filter(grupo_id=id)
    todos_los_estudiantes = Estudiante.objects.all()          
    contexto = {
        'grupo': grupo,
        'estudiantes': lista_estudiantes,
        'todos_los_estudiantes' :  todos_los_estudiantes,
        'profesores': profesores
     
    }
    return render(request, 'app/adminvergrupo.html', contexto)

def admineditargrupo(request, id):
    grupo = Grupo.objects.get(id=id)
    codigo = request.POST['codigo']
    asignatura = request.POST['asignatura']
    profesor = request.POST['profesor']
    semestre = request.POST['semestre']

    grupo.codigo = codigo
    grupo.asignatura = asignatura
    grupo.semestre = semestre
    grupo.profesor_id = profesor
    grupo.save()

    return redirect('app:admingrupo')

def eliminarestudiante(request, id):
    
     estudiante = EstudianteGrupo.objects.get(id=id)
     grupo = estudiante.grupo_id

     estudiante.delete()

     return redirect('app:adminagregarestudiantegrupo', grupo)


def agregarestudiante(request, id):
    estudiante = request.POST['estudianteselect']
    asignar = EstudianteGrupo()
    asignar.estudiante_id = estudiante
    asignar.grupo_id = id
    asignar.save()
    return redirect('app:adminagregarestudiantegrupo',id)

def adminagregarestudiantegrupo(request, id):
    lista_estudiantes = EstudianteGrupo.objects.filter(grupo_id=id) 
    estudiantes_todos = Estudiante.objects.all()
    
    contexto = {
        'estudiantes': lista_estudiantes,
        'idgrupo': id,
        'estudiantes_todos': estudiantes_todos
    }
    
    return render(request, 'app/adminagregarestudiantegrupo.html', contexto)
   


def adminprofesores(request):
    lista_prof = Profesor.objects.all()
    contexto = {
        'prof_list': lista_prof
    }
    return render(request, 'app/adminprofesores.html', contexto)

def adminprofesorescrear(request):
    nombre = request.POST['nombre']
    apellido = request.POST['apellido']
    email = request.POST['email']
    documento = request.POST['documento']
    username = request.POST['username']
    catedra = 'catedra' in request.POST
    usuario = User()
    usuario.first_name = nombre
    usuario.last_name = apellido
    usuario.email = email
    usuario.set_password(documento)
    usuario.username = username
    usuario.save()
    profesor = Profesor()
    profesor.catedra = catedra
    profesor.user = usuario
    profesor.save()

    return redirect('app:adminprofesores')
    
def adminverprofesor(request, id):
    profesor = Profesor.objects.get(user_id=id) 
    lista_grupos = Grupo.objects.filter(profesor_id=id)  
    
     
    contexto = {
        'profesor': profesor,
        'grupos': lista_grupos,
        
    }

    return render(request, 'app/adminverprofesor.html', contexto)
        
def profesor(request):
    return render(request, 'app/profesor.html')

def profveract(request):
    profesor = request.user.id
    grupo = Grupo.objects.filter(profesor_id = profesor)
    actividad = Actividad.objects.all()
    contexto = {
        'grupos' : grupo,
        'actividades' : actividad
    }
    return render(request, 'app/profveract.html', contexto)

def profveractcrear(request):
    descripcion = request.POST['descripcion']
    grupo = request.POST['grupo']
    fecha = request.POST['fecha']
    actividad = Actividad()
    actividad.fecha_creacion = fecha
    actividad.grupo_id = grupo
    actividad.descripcion = descripcion
    actividad.save()
    

    return redirect('app:profveract')   
    
def veractividad(request, id):
    actividad = Actividad.objects.get(id=id)
    estudiantes = EstudianteGrupo.objects.all()
    notas = Nota.objects.all()

    contexto = {
        'notas': notas,
        'estudiantes': estudiantes,
        'actividad': actividad,
    }
    return render(request, 'app/veractividad.html', contexto)
    
def profnota(request,id1,id2):
    info=EstudianteGrupo.objects.get(id=id2)
    acti=Actividad.objects.get(id = id1)
    contexto = {
        'info': info,
        'actividad':acti
    }
    return render(request, 'app/profnota.html', contexto)

def guardarnota(request, id1, id2):

    valor = request.POST['nota']
    if Nota.objects.filter(Q(actividad_id = id1) & Q(estudiantegrupo_id = id2)):
        nota = Nota.objects.get(actividad_id = id1)
        nota.actividad_id = id1
        nota.valor = valor
        nota.estudiantegrupo_id = id2
        nota.save()
    else:
       nota = Nota()
       nota.actividad_id = id1
       nota.valor = valor
       nota.estudiantegrupo_id = id2
       nota.save()

    return redirect('app:veractividad', id1)
    

def estudiantes(request):
    idestudiante = request.user.id
    grupos = EstudianteGrupo.objects.filter(estudiante_id = idestudiante)
    contexto = {

         'grupos': grupos
    }
    return render(request, 'app/estudiantes.html', contexto)

    

def estugrupo(request, id):
    grupo = EstudianteGrupo.objects.get(id = id)
    actividades = Actividad.objects.filter(grupo_id = grupo.grupo_id)     
    notas = Nota.objects.filter(estudiantegrupo_id = id)     
    contexto = {
         'grupo': grupo,
         'actividades':actividades, 
         'notas':notas,
    }
    return render(request, 'app/estugrupo.html', contexto)
    

def proflistagrupos(request):
    profesor = request.user.id
    grupos = Grupo.objects.filter(profesor_id = profesor)
    
    contexto = {
        'proflista_grupos': grupos
        
    }

    return render(request, 'app/proflistagrupos.html', contexto)


def profactgrupo(request):
    lista_grupos = Grupo.objects.all()
    contexto = {
        'proflista_grupos': lista_grupos
        
    }

    return render(request, 'app/profactgrupo.html', contexto)
    
def profvergrupo(request, id):
    grupo = Grupo.objects.get(id=id)
    actividades = Actividad.objects.filter(grupo_id=id)
    estudiantes = EstudianteGrupo.objects.filter(grupo_id=id)
    contexto = {
        'info_grupo' : grupo,
        'actividades': actividades,
        'estudiantes': estudiantes
    }
    return render(request, 'app/profvergrupo.html', contexto)
     

def profeactynota(request, id):
    notas = Nota.objects.filter(estudiantegrupo_id=id)
    estudiante = EstudianteGrupo.objects.get(id=id)

    contexto = {
        'nota': notas,
        'estudiante': estudiante   
    } 

    return render(request, 'app/profeactynota.html', contexto)
    

def form_login(request):
    return render(request, 'app/index.html')

def iniciar_sesion(request):
    u = request.POST['username']
    p = request.POST['password']


    usuario = authenticate(username=u, password=p)

    if usuario is not None:
        login(request, usuario)

        us = User.objects.get(username = u)

        print(us.is_superuser)

        if us.is_superuser == True:
            return redirect('app:admin')
        else:

            if Estudiante.objects.filter(user_id = us.id)  :
                    return redirect('app:estudiantes')
            else:
                return redirect('app:profesor')

        return render(request, 'app/index.html', contexto)

    else:
        contexto = {
            'error': 'Credenciales no válidas'
        }
        return render(request, 'app/index.html', contexto)

def cerrar_sesion(request):

    logout(request)

    return redirect('app:form_login')

